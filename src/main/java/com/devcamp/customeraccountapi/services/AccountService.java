package com.devcamp.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccountapi.models.Account;
@Service
public class AccountService extends CustomerService{
    Account account1 = new Account(1001, customer1, 10000000);
    Account account2 = new Account(1002, customer2, 200000000);
    Account account3 = new Account(1003, customer3, 50000000);
    public ArrayList<Account> getAllAccounts(){
        ArrayList<Account> accountList = new ArrayList<>();
        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);
        return accountList;

    }
}
