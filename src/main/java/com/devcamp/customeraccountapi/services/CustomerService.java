package com.devcamp.customeraccountapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccountapi.models.Customer;
@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "Linh", 20);
    Customer customer2 = new Customer(2, "Minh", 10);
    Customer customer3 = new Customer(2, "Trang", 20);
    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        return customerList; 
    }
}
